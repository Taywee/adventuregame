extends Camera

# The target node, which will be looked at
export(NodePath) var look_target_: NodePath
onready var look_target: Spatial = get_node(look_target_)
onready var look_position: Vector3 = look_target.global_transform.origin

export(float) var seek_speed: float = 3.0
export(float) var look_speed: float = 3.0
export(float) var rotate_speed: float = 2.0
export(float) var zoom_speed: float = 0.5

var zoom_position: float = 0.5

# Height in meters above the look point
var height: float =  2.0

# Horizontal distance from the look point
var distance: float =  5.0

func _process(delta: float):
	var horizontal = Input.get_action_strength('camera_right') - Input.get_action_strength('camera_left')
	var vertical = Input.get_action_strength('camera_up') - Input.get_action_strength('camera_down')

	var seek_delta: float = min(delta * seek_speed, 1.0) 
	var rotate_delta: float = clamp(delta * horizontal * rotate_speed, -1.0, 1.0) 
	var zoom_delta: float = clamp(delta * vertical * zoom_speed, -1.0, 1.0) 
	zoom_position = clamp(zoom_position - zoom_delta, 0.0, 1.0)
	height = lerp(-0.5, 4.0, zoom_position)
	distance = lerp(1.0, 5.0, zoom_position)

	var seek_distance = global_transform.origin - look_target.global_transform.origin 
	seek_distance.y = 0
	seek_distance = seek_distance.normalized() * distance
	seek_distance.y = height
	var seek_target = look_target.global_transform.origin + seek_distance

	global_transform.origin = lerp(global_transform.origin, seek_target, seek_delta)

	# Rotate position based on horizontal
	var current_distance = global_transform.origin - look_target.global_transform.origin 
	global_transform.origin = look_target.global_transform.origin + current_distance.rotated(Vector3.UP, -rotate_delta)

	var look_delta: float = min(delta * look_speed, 1.0) 
	look_position = lerp(look_position, look_target.global_transform.origin, look_delta)
	look_at(look_position, Vector3.UP)
