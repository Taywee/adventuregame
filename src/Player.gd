extends KinematicBody


export(float) var speed = 5.0
export(float) var jump_speed = 5.0
export(NodePath) var camera_: NodePath
onready var camera: Camera = get_node(camera_)
onready var animation_player: AnimationPlayer = $Pivot/female/AnimationPlayer

var gravity = ProjectSettings.get_setting('physics/3d/default_gravity') * ProjectSettings.get_setting('physics/3d/default_gravity_vector')
var up: Vector3 = (-gravity).normalized()
var velocity = Vector3.ZERO

enum AnimationState {
	IDLE,
	WALK,
}

var animation_state = AnimationState.IDLE

# Called when the node enters the scene tree for the first time.
func _ready():
	animation_player.get_animation('Walk').loop = true
	animation_player.stop(true)

func _physics_process(delta: float):
	var horizontal = Input.get_action_strength('move_right') - Input.get_action_strength('move_left')
	var vertical = Input.get_action_strength("move_backward") - Input.get_action_strength('move_forward') 
	var camera_basis = camera.global_transform.basis

	# TODO: fix this for horizontal.  The player's speed doesn't work if the
	# camera is looking down at the player.
	var movement = horizontal * camera_basis[0] + vertical * camera_basis[2]
	var new_animation_state = AnimationState.IDLE
	movement.y = 0

	velocity += gravity * delta

	if is_on_floor():
		var movement_length = movement.length()
		if movement_length > 1.0:
			movement /= movement_length

		#look_at(transform.origin + movement * speed, up)

		velocity.x = movement.x * speed
		velocity.z = movement.z * speed
		if movement_length > 0.02:
			rotation.y = atan2(velocity.x, velocity.z)
			animation_player.playback_speed = movement_length
			new_animation_state = AnimationState.WALK
			
		if Input.is_action_just_pressed('jump'):
			animation_player.stop(true)
			velocity.y = jump_speed
			new_animation_state = AnimationState.IDLE
	
	if animation_state != new_animation_state:
		animation_state = new_animation_state
		if animation_state == AnimationState.IDLE:
			animation_player.stop(true)
		elif animation_state == AnimationState.WALK:
			animation_player.play("Walk")

	velocity = move_and_slide(velocity, up)
